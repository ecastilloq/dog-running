class CreateDog < ActiveRecord::Migration[5.2]
  def change
    create_table :dogs do |t|
      t.string :name
      t.references :breed, foreign_key: true
      t.integer :age
      t.integer :weight
      t.timestamp :reservation_date_limit
    end
  end
end
