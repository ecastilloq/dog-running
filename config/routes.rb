Rails.application.routes.draw do
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html

  namespace :api do
    namespace :v1 do
      resources :breeds, only: [:index]
      resources :dogs
      resources :reservations, only: [:create]
    end
  end
  # TODO: remove ActiveStorage routes

end
