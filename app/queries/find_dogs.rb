# frozen_string_literal: true

class FindDogs
  DEFAULT_PAGE_SIZE = 10
  DEFAULT_PAGE_NUMBER = 1
  SORT_REGEX = /(asc|desc)\((breed|age|name|weight)\)/.freeze

  def initialize(params)
    @params = params
  end

  def call
    scoped = Dog.includes(:breed).left_outer_joins(:reservations).all
    scoped = filter(scoped)
    { dogs: paginate(sort(scoped)), meta: build_meta(scoped) }
  end

  private

  def filter(scoped)
    # TODO: add greater and less than filters for age and weight
    where_clauses = []
    filter_by_name(where_clauses)
    filter_by_age(where_clauses)
    filter_by_weight(where_clauses)
    filter_by_reserved(where_clauses)
    scoped.where(where_clauses.join(' AND '))
  end

  def filter_by_name(where_clauses)
    where_clauses << "dogs.name ILIKE '%#{@params[:name]}%'" if @params[:name].present?
  end

  def filter_by_age(where_clauses)
    where_clauses << "dogs.age = #{@params[:age]}" if @params[:age].present?
  end

  def filter_by_weight(where_clauses)
    where_clauses << "dogs.weight = #{@params[:weight]}" if @params[:weight].present?
  end

  def filter_by_reserved(where_clauses)
    return unless @params.key?(:reserved)

    clause = "(reservations.id IS NULL OR TO_DATE('#{Time.zone.today}', 'YYYY-MM-DD') NOT BETWEEN " \
        'reservations.start_date AND reservations.end_date)'
    if ActiveModel::Type::Boolean.new.cast(@params[:reserved])
      clause = "TO_DATE('#{Time.zone.today}', 'YYYY-MM-DD') BETWEEN reservations.start_date AND reservations.end_date"
    end
    where_clauses << clause
  end

  def sort(scoped)
    sort_clause = parse_sort_param
    sort_clause = sort_clause.empty? ? 'dogs.name ASC' : sort_clause
    scoped.order(sort_clause)
  end

  def paginate(scoped)
    per_page = positive_value_or_default(@params[:per_page], DEFAULT_PAGE_SIZE)
    page_number = positive_value_or_default(@params[:page], DEFAULT_PAGE_NUMBER)
    scoped.paginate(per_page: per_page, page: page_number)
  end

  def positive_value_or_default(param, default)
    value = param.to_i
    value.positive? ? value : default
  end

  def parse_sort_param
    # Valid examples of sort params are asc(breed) or asc(age),desc(name)
    sort_params = @params[:sort] || ''
    valid_sort_params = sort_params.downcase.split(',').map { |entry| SORT_REGEX.match(entry) }.compact
    valid_sort_params.map { |entry| parse_sort_entry(entry) }.join(', ')
  end

  def parse_sort_entry(entry)
    sortable_key = entry[2]
    sortable_key = sortable_key == 'breed' ? 'breeds.name' : "dogs.#{sortable_key}"
    "#{sortable_key} #{entry[1].upcase}"
  end

  def build_meta(scoped)
    age_average = scoped.average(:age)
    weight_average = scoped.average(:weight)
    breed_count = scoped.group('breeds.name').order('breeds.name').count
    { age_average: age_average, weight_average: weight_average, breed_count: breed_count }
  end
end
