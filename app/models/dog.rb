# frozen_string_literal: true

# == Schema Information
#
# Table name: dogs
#
#  id                     :bigint(8)        not null, primary key
#  name                   :string
#  breed_id               :bigint(8)
#  age                    :integer
#  weight                 :integer
#  reservation_date_limit :datetime
#

class Dog < ApplicationRecord
  belongs_to :breed
  has_many :reservations, dependent: :destroy

  validates :name, presence: true
  validates :breed, presence: true
  validates :age, presence: true, numericality: { greater_than_or_equal_to: 1, only_integer: true }
  validates :weight, presence: false, numericality: { greater_than_or_equal_to: 1 }, allow_nil: true
end
