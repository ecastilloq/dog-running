# frozen_string_literal: true

# == Schema Information
#
# Table name: breeds
#
#  id   :bigint(8)        not null, primary key
#  name :string
#

class Breed < ApplicationRecord
  has_many :dogs, dependent: :destroy
  validates :name, presence: true, uniqueness: true
end
