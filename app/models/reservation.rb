# frozen_string_literal: true

# == Schema Information
#
# Table name: reservations
#
#  id         :bigint(8)        not null, primary key
#  start_date :date
#  end_date   :date
#  dog_id     :bigint(8)
#

class Reservation < ApplicationRecord
  belongs_to :dog

  validates :start_date, presence: true
  validates :end_date, presence: true
  validates :dog, presence: true
  validate :dog_reservable

  private

  def dog_reservable
    reserved = Reservation.where('(start_date, end_date) OVERLAPS (?, ?)', start_date, end_date)
                          .where(dog_id: dog_id).any?
    errors.add(:dog, 'is reserved') if reserved
  end
end
