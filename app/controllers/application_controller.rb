# frozen_string_literal: true

class ApplicationController < ActionController::API
  rescue_from ActiveRecord::RecordNotFound do |_exception|
    render json: { error: 'Not found' }, status: :not_found
  end

  protected

  def build_error_message(model)
    { errors: model.errors.full_messages }
  end
end
