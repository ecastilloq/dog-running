# frozen_string_literal: true

module Api
  module V1
    class DogsController < ApplicationController
      before_action :read_or_load_dog, only: %i[show update]

      def index
        dog_list = FindDogs.new(search_params).call
        serialized_dogs =
          ActiveModelSerializers::SerializableResource.new(dog_list[:dogs], each_serializer: DogSerializer).as_json
        render json: { dogs: serialized_dogs, meta: dog_list[:meta] }
      end

      def create
        @dog = Dog.new(dog_params)
        if @dog.save
          Rails.cache.write(@dog.cache_key, @dog, expires_in: 5.hours)
          render json: @dog, status: :created
        else
          render json: build_error_message(@dog), status: :bad_request
        end
      end

      def show
        render json: @dog
      end

      def update
        if @dog.update(dog_params)
          Rails.cache.write(@dog.cache_key, @dog, expires_in: 5.hours)
          render json: @dog
        else
          render json: build_error_message(@dog), status: :bad_request
        end
      end

      def destroy
        if Dog.destroy(params[:id])
          Rails.cache.delete("dogs/#{params[:id]}")
          render status: :ok
        else
          render json: { error: 'Falid to delete dog' }, status: :server_error
        end
      end

      private

      def read_or_load_dog
        @dog = Rails.cache.fetch("dogs/#{params[:id]}", expires_in: 5.hours) { Dog.includes(:breed).find(params[:id]) }
      end

      def dog_params
        params.require(:dog).permit(:name, :breed_id, :age, :weight, :reservation_date_limit)
      end

      def search_params
        params.permit(:name, :age, :reserved, :weight, :sort, :page, :per_page)
      end
    end
  end
end
