# frozen_string_literal: true

module Api
  module V1
    class BreedsController < ApplicationController
      def index
        @breeds = Breed.all
        render json: @breeds
      end
    end
  end
end
