# frozen_string_literal: true

module Api
  module V1
    class ReservationsController < ApplicationController
      def create
        dog_id = params[:dog_id]
        number_of_days = params[:number_of_days]
        if number_of_days.nil? || number_of_days.to_i <= 0
          return render json: { error: 'Number of days is required and greater than 0' }, status: :bad_request
        end

        start_date = Time.zone.today
        end_date = start_date + number_of_days.to_i.days
        @reservation = Reservation.new(dog_id: dog_id, start_date: start_date, end_date: end_date)
        if @reservation.save
          render json: @reservation, include: [dog: :breed], status: :created
        else
          render json: build_error_message(@reservation), status: :bad_request
        end
      end
    end
  end
end
