# frozen_string_literal: true

# == Schema Information
#
# Table name: dogs
#
#  id                     :bigint(8)        not null, primary key
#  name                   :string
#  breed_id               :bigint(8)
#  age                    :integer
#  weight                 :integer
#  reservation_date_limit :datetime
#

class DogSerializer < ActiveModel::Serializer
  attributes :id, :name, :age, :weight, :reservation_date_limit
  belongs_to :breed
end
