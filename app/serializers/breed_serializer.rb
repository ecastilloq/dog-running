# frozen_string_literal: true

# == Schema Information
#
# Table name: breeds
#
#  id   :bigint(8)        not null, primary key
#  name :string
#

class BreedSerializer < ActiveModel::Serializer
  attributes :id, :name
end
