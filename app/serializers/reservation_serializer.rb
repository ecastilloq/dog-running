# frozen_string_literal: true

# == Schema Information
#
# Table name: reservations
#
#  id         :bigint(8)        not null, primary key
#  start_date :date
#  end_date   :date
#  dog_id     :bigint(8)
#

class ReservationSerializer < ActiveModel::Serializer
  attributes :id, :start_date, :end_date
  belongs_to :dog
end
