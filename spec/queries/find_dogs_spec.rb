# frozen_string_literal: true

describe FindDogs do
  let!(:dog_list) { create_list(:dog, 25) }
  subject { described_class.new(query_params).call }

  context 'when there are no params' do
    let(:query_params) { {} }

    it 'queries with the defaults' do
      dogs_sql = subject[:dogs].to_sql
      expect(dogs_sql).to include('FROM "dogs" LEFT OUTER JOIN "reservations" ON "reservations"."dog_id" = "dogs"."id"')
      expect(dogs_sql).to include('ORDER BY dogs.name ASC')
      expect(dogs_sql).to include('LIMIT 10 OFFSET 0')
      expect(subject[:dogs]).to eq(dog_list.sort_by(&:name).first(10))
      expect(subject[:meta][:age_average].to_f).to eq(age_average(dog_list))
      expect(subject[:meta][:weight_average].to_f).to eq(weight_average(dog_list))
      expect(subject[:meta][:breed_count]).to eq(breed_count(dog_list))
    end
  end

  context 'when filter params are present' do
    context 'when querying by name' do
      let(:names) { ['Fake 1', 'Test 2', 'Test 1', 'fake 2', 'FAKE 4'] }
      let!(:dog_list) { names.map { |n| create(:dog, name: n) } }
      let(:name_query) { 'fak' }
      let(:query_params) { { name: name_query } }

      it 'finds dogs which name is similar to query (case insentitve)' do
        expect(subject[:dogs].to_sql).to include("WHERE (dogs.name ILIKE '%#{name_query}%')")
        matching_dogs = dog_list.find_all { |d| d.name.downcase.include?(name_query.downcase) }
        expect(subject[:dogs]).to eq(matching_dogs.sort_by(&:name).first(10))
        expect(subject[:meta][:age_average].to_f).to eq(age_average(matching_dogs))
        expect(subject[:meta][:weight_average].to_f).to eq(weight_average(matching_dogs))
        expect(subject[:meta][:breed_count]).to eq(breed_count(matching_dogs))
      end
    end

    context 'when querying by age' do
      let(:ages) { [1, 4, 3, 3, 5, 3, 2, 3, 7, 4, 3] }
      let!(:dog_list) { ages.map { |a| create(:dog, age: a) } }
      let(:age_query) { 3 }
      let(:query_params) { { age: age_query } }

      it 'finds dog which age matches query' do
        expect(subject[:dogs].to_sql).to include("WHERE (dogs.age = #{age_query})")
        matching_dogs = dog_list.find_all { |d| d.age == age_query }
        expect(subject[:dogs]).to eq(matching_dogs.sort_by(&:name).first(10))
        expect(subject[:meta][:age_average].to_f).to eq(age_average(matching_dogs))
        expect(subject[:meta][:weight_average].to_f).to eq(weight_average(matching_dogs))
        expect(subject[:meta][:breed_count]).to eq(breed_count(matching_dogs))
      end
    end

    context 'when querying by weight' do
      let(:weights) { [100, 200, 300, 100, 100, 200, 200, 100, 200, 400, 300] }
      let!(:dog_list) { weights.map { |w| create(:dog, weight: w) } }
      let(:weight_query) { 100 }
      let(:query_params) { { weight: weight_query } }

      it 'finds dog which weight matches query' do
        expect(subject[:dogs].to_sql).to include("WHERE (dogs.weight = #{weight_query})")
        matching_dogs = dog_list.find_all { |d| d.weight == weight_query }
        expect(subject[:dogs]).to eq(matching_dogs.sort_by(&:name).first(10))
        expect(subject[:meta][:age_average].to_f).to eq(age_average(matching_dogs))
        expect(subject[:meta][:weight_average].to_f).to eq(weight_average(matching_dogs))
        expect(subject[:meta][:breed_count]).to eq(breed_count(matching_dogs))
      end
    end

    context 'when querying by reserved' do
      let!(:reserved_dogs) { create_list(:dog, 5) }
      let!(:reservations) { reserved_dogs.each { |d| create(:reservation, dog: d) } }

      context 'when reserved is true' do
        let(:query_params) { { reserved: true } }

        it 'finds reserved dogs' do
          query = "WHERE (TO_DATE('#{Time.zone.today}', 'YYYY-MM-DD') BETWEEN " \
            'reservations.start_date AND reservations.end_date)'
          expect(subject[:dogs].to_sql).to include(query)
          expect(subject[:dogs]).to eq(reserved_dogs.sort_by(&:name).first(10))
          expect(subject[:meta][:age_average].to_f).to eq(age_average(reserved_dogs))
          expect(subject[:meta][:weight_average].to_f).to eq(weight_average(reserved_dogs))
          expect(subject[:meta][:breed_count]).to eq(breed_count(reserved_dogs))
        end
      end

      context 'when reserved is false' do
        let(:query_params) { { reserved: false } }

        it 'finds not reserved dogs' do
          query = "WHERE ((reservations.id IS NULL OR TO_DATE('#{Time.zone.today}', 'YYYY-MM-DD') NOT BETWEEN " \
            'reservations.start_date AND reservations.end_date))'
          expect(subject[:dogs].to_sql).to include(query)
          expect(subject[:dogs]).to eq(dog_list.sort_by(&:name).first(10))
          expect(subject[:meta][:age_average].to_f).to eq(age_average(dog_list))
          expect(subject[:meta][:weight_average].to_f).to eq(weight_average(dog_list))
          expect(subject[:meta][:breed_count]).to eq(breed_count(dog_list))
        end
      end
    end

    context 'when querying by multiple filters' do
      let(:name_query) { 'fak' }
      let(:age_query) { 3 }
      let(:query_params) { { name: name_query, age: age_query } }

      it 'finds dogs that match all filters' do
        expect(subject[:dogs].to_sql).to include("WHERE (dogs.name ILIKE '%#{name_query}%' AND dogs.age = #{age_query}")
      end
    end
  end

  context 'when sort params are present' do
    context 'when invalid params' do
      let(:query_params) { { sort: 'invalid' } }

      it 'sorts with the defaults' do
        expect(subject[:dogs].to_sql).to include('ORDER BY dogs.name ASC')
        expect(subject[:dogs]).to eq(dog_list.sort_by(&:name).first(10))
      end
    end

    context 'when name is present' do
      let(:query_params) { { sort: 'desc(name)' } }

      it 'sorts by name' do
        expect(subject[:dogs].to_sql).to include('ORDER BY dogs.name DESC')
        expect(subject[:dogs]).to eq(dog_list.sort_by(&:name).reverse.first(10))
      end
    end

    context 'when breed is present' do
      let(:query_params) { { sort: 'asc(breed)' } }

      it 'sorts by breed name' do
        expect(subject[:dogs].to_sql).to include('ORDER BY breeds.name ASC')
        expect(subject[:dogs]).to eq(dog_list.sort_by { |d| d.breed.name }.first(10))
      end
    end

    context 'when age is present' do
      let(:ages) { [1, 5, 7, 4, 8] }
      let!(:dog_list) { ages.map { |a| create(:dog, age: a) } }
      let(:query_params) { { sort: 'desc(age)' } }

      it 'sorts by age' do
        expect(subject[:dogs].to_sql).to include('ORDER BY dogs.age DESC')
        expect(subject[:dogs]).to eq(dog_list.sort_by(&:age).reverse)
      end
    end

    context 'when sorting by weight' do
      let(:weights) { [215, 326, 154, 289, 98] }
      let!(:dog_list) { weights.map { |w| create(:dog, weight: w) } }
      let(:query_params) { { sort: 'asc(weight)' } }

      it 'sorts by weight' do
        expect(subject[:dogs].to_sql).to include('ORDER BY dogs.weight ASC')
        expect(subject[:dogs]).to eq(dog_list.sort_by(&:weight))
      end
    end

    context 'when multiple sort params are present' do
      let(:query_params) { { sort: 'desc(weight),asc(breed)' } }

      it 'sorts by all present params' do
        expect(subject[:dogs].to_sql).to include('ORDER BY dogs.weight DESC, breeds.name ASC')
      end
    end
  end

  context 'when paginate params are present' do
    context 'when invalid params' do
      let(:query_params) { { per_page: 0, page: -1 } }

      it 'paginates with the defaults' do
        expect(subject[:dogs].to_sql).to include('LIMIT 10 OFFSET 0')
        expect(subject[:dogs]).to eq(dog_list.sort_by(&:name).first(10))
      end
    end

    context 'when page is present' do
      let(:page_number) { 2 }
      let(:query_params) { { page: page_number } }

      it 'paginates with per_page default and specified page' do
        offset = 10 * (page_number - 1)
        expect(subject[:dogs].to_sql).to include("LIMIT 10 OFFSET #{offset}")
        expect(subject[:dogs]).to eq(dog_list.sort_by(&:name).slice(offset, 10))
      end
    end

    context 'when per_page is present' do
      let(:per_page_amount) { 3 }
      let(:query_params) { { per_page: per_page_amount } }

      it 'paginates with per_page default and specified page' do
        expect(subject[:dogs].to_sql).to include("LIMIT #{per_page_amount} OFFSET 0")
        expect(subject[:dogs]).to eq(dog_list.sort_by(&:name).first(per_page_amount))
      end
    end
  end
end
