# frozen_string_literal: true

# == Schema Information
#
# Table name: breeds
#
#  id   :bigint(8)        not null, primary key
#  name :string
#

FactoryBot.define do
  factory :breed do
    sequence(:name) { |n| "Breed #{n}" }
  end
end
