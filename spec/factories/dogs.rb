# frozen_string_literal: true

# == Schema Information
#
# Table name: dogs
#
#  id                     :bigint(8)        not null, primary key
#  name                   :string
#  breed_id               :bigint(8)
#  age                    :integer
#  weight                 :integer
#  reservation_date_limit :datetime
#

FactoryBot.define do
  factory :dog do
    sequence(:name) { |n| "Dog #{n}" }
    breed
    age { rand(1..15) }
    weight { rand(1..300) }
  end
end
