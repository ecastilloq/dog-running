# frozen_string_literal: true

# == Schema Information
#
# Table name: reservations
#
#  id         :bigint(8)        not null, primary key
#  start_date :date
#  end_date   :date
#  dog_id     :bigint(8)
#

FactoryBot.define do
  factory :reservation do
    dog
    start_date { Time.zone.now }
    end_date { Time.zone.now + 2.days }
  end
end
