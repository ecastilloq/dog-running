# frozen_string_literal: true

require 'rails_helper'

describe 'API V1 Dogs', type: :request do
  describe 'GET /api/v1/dogs' do
    let!(:dog_list) { create_list(:dog, 9) }
    before(:each) { get api_v1_dogs_path }
    subject { response }

    it 'should respond with dog list' do
      should be_successful
      json_response = JSON.parse(response.body)
      expect(json_response).to include('dogs')
      expect(json_response).to include('meta')
      expect(json_response['dogs'].to_a.size).to eq(dog_list.size)
      expect(json_response['meta']).to include('age_average')
      expect(json_response['meta']).to include('weight_average')
      expect(json_response['meta']).to include('breed_count')
    end
  end

  describe 'POST /api/v1/dogs' do
    subject { response }

    context 'when params are invalid' do
      before(:each) { post api_v1_dogs_path, params: body }
      let(:body) { { dog: { name: 'fake_name' } } }

      it { should be_a_bad_request }
    end

    context 'when params are valid' do
      let(:breed) { create(:breed) }
      let(:dog) { build(:dog, breed: breed) }
      let(:body) { { dog: dog.attributes } }

      it 'should create a dog' do
        expect { post api_v1_dogs_path, params: body }.to change(Dog, :count).by(1)
        should be_a_created
        json_response = JSON.parse(response.body)
        expect(json_response).to include('name' => dog.name)
        expect(json_response).to include('age' => dog.age)
        expect(json_response).to include('weight' => dog.weight)
        expect(json_response).to include('breed')
        expect(json_response['breed']).to include('id' => breed.id)
        expect(json_response['breed']).to include('name' => breed.name)
      end
    end
  end

  describe 'GET /api/v1/dogs/{id}' do
    let!(:dog) { create(:dog) }
    subject { response }

    context 'when dog does not exist' do
      before(:each) { get api_v1_dog_path(0) }

      it { should be_a_not_found }
    end

    context 'when params are valid' do
      it 'should show dog' do
        get api_v1_dog_path(dog)
        should be_successful
        json_response = JSON.parse(response.body)
        expect(json_response).to include('name' => dog.name)
        expect(json_response).to include('age' => dog.age)
        expect(json_response).to include('weight' => dog.weight)
        expect(json_response).to include('breed')
        expect(json_response['breed']).to include('id' => dog.breed.id)
        expect(json_response['breed']).to include('name' => dog.breed.name)
      end
    end
  end

  describe 'PUT /api/v1/dogs/{id}' do
    let!(:dog) { create(:dog) }
    subject { response }

    context 'when params are invalid' do
      before(:each) { put api_v1_dog_path(dog), params: body }
      let(:body) { { dog: { breed_id: 0 } } }

      it { should be_a_bad_request }
    end

    context 'when dog does not exist' do
      before(:each) { put api_v1_dog_path(0), params: body }

      it { should be_a_not_found }
    end

    context 'when params are valid' do
      let(:breed) { create(:breed) }
      let(:dog_update_details) { build(:dog, breed: breed).attributes }
      let(:body) { { dog: dog_update_details } }

      it 'should update dog' do
        expect { put api_v1_dog_path(dog), params: body }.not_to change(Dog, :count)
        should be_successful
        json_response = JSON.parse(response.body)
        expect(json_response).to include('name' => dog_update_details['name'])
        expect(json_response).to include('age' => dog_update_details['age'])
        expect(json_response).to include('weight' => dog_update_details['weight'])
        expect(json_response).to include('breed')
        expect(json_response['breed']).to include('id' => breed.id)
        expect(json_response['breed']).to include('name' => breed.name)
      end
    end
  end

  describe 'DELETE /api/v1/dogs/{id}' do
    subject { response }

    context 'when dog does not exist' do
      before(:each) { delete api_v1_dog_path(0), params: body }

      it { should be_a_not_found }
    end

    context 'when dog exists' do
      let!(:dog) { create(:dog) }

      it 'should delete dog' do
        expect { delete api_v1_dog_path(dog) }.to change(Dog, :count).by(-1)
        should be_successful
      end
    end
  end
end
