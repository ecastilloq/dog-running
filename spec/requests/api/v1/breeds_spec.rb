# frozen_string_literal: true

require 'rails_helper'

describe 'API V1 Breeds', type: :request do
  describe 'GET breeds' do
    let!(:breed_list) { create_list(:breed, 15) }

    before(:each) { get api_v1_breeds_path }
    subject { response }

    it 'should respond with all breeds' do
      should be_successful
      expect(subject.body).to eq(breed_list.to_json)
    end
  end
end
