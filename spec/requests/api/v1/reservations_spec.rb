# frozen_string_literal: true

require 'rails_helper'

describe 'API V1 Reservations', type: :request do
  describe 'POST /api/v1/reservations' do
    subject { response }

    context 'when params are invalid' do
      before(:each) { post api_v1_reservations_path, params: body }
      context 'when number of days is not present' do
        let(:body) { { dog_id: 1 } }

        it { should be_a_bad_request }
      end

      context 'when number of days is 0' do
        let(:body) { { number_of_days: 0, dog_id: 1 } }

        it { should be_a_bad_request }
      end

      context 'when dog id is not present' do
        let(:body) { { number_of_days: 3 } }

        it { should be_a_bad_request }
      end
    end

    context 'when params are valid' do
      let(:dog) { create(:dog) }
      let(:number_of_days) { 2 }
      let(:body) { { number_of_days: number_of_days, dog_id: dog.id } }

      it 'should create reservation' do
        expect { post api_v1_reservations_path, params: body }.to change(Reservation, :count).by(1)
        should be_a_created
        json_response = JSON.parse(response.body)
        expect(json_response).to include('start_date' => Time.zone.today.to_s)
        expect(json_response).to include('end_date' => (Time.zone.today + number_of_days).to_s)
        expect(json_response).to include('dog')
        expect(json_response['dog']).to include('id' => dog.id)
      end
    end
  end
end
