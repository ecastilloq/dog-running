# frozen_string_literal: true

module DogHelpers
  def age_average(dog_list)
    dog_list.sum(&:age) / dog_list.size.to_f
  end

  def weight_average(dog_list)
    dog_list.sum(&:weight) / dog_list.size.to_f
  end

  def breed_count(dog_list)
    dog_list.group_by(&:breed).map { |b, d| { b.name => d.size } }.reduce(&:merge)
  end
end
