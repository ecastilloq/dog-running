# frozen_string_literal: true

describe DogSerializer do
  let(:reservation) { create(:reservation) }
  subject { serialize(reservation) }

  it { should include(id: reservation.id) }
  it { should include(start_date: reservation.start_date) }
  it { should include(end_date: reservation.end_date) }
  it { should include(:dog) }
end
