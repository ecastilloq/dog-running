# frozen_string_literal: true

describe BreedSerializer do
  let(:breed) { create(:breed) }
  subject { serialize(breed) }

  it { should include(id: breed.id) }
  it { should include(name: breed.name) }
end
