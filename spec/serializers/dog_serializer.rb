# frozen_string_literal: true

describe DogSerializer do
  let(:dog) { create(:dog) }
  subject { serialize(dog) }

  it { should include(id: dog.id) }
  it { should include(name: dog.name) }
  it { should include(weight: dog.weight) }
  it { should include(reservation_date_limit: dog.reservation_date_limit) }
  it { should include(breed: { id: dog.breed.id, name: dog.breed.name }) }
end
