# frozen_string_literal: true

# == Schema Information
#
# Table name: breeds
#
#  id   :bigint(8)        not null, primary key
#  name :string
#

describe Breed, type: :model do
  it { should have_many(:dogs) }
  it { should validate_presence_of(:name) }
  it { should validate_uniqueness_of(:name) }
end
