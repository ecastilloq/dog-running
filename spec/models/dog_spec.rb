# frozen_string_literal: true

# == Schema Information
#
# Table name: dogs
#
#  id                     :bigint(8)        not null, primary key
#  name                   :string
#  breed_id               :bigint(8)
#  age                    :integer
#  weight                 :integer
#  reservation_date_limit :datetime
#

describe Dog, type: :model do
  it { should belong_to(:breed) }
  it { should have_many(:reservations) }
  it { should validate_presence_of(:name) }
  it { should validate_presence_of(:breed) }
  it { should validate_presence_of(:age) }
  it { should validate_numericality_of(:age).is_greater_than_or_equal_to(1) }
  it { should_not validate_presence_of(:weight) }
  it { should validate_numericality_of(:weight).is_greater_than_or_equal_to(1) }
end
