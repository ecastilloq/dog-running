# frozen_string_literal: true

# == Schema Information
#
# Table name: reservations
#
#  id         :bigint(8)        not null, primary key
#  start_date :date
#  end_date   :date
#  dog_id     :bigint(8)
#

describe Reservation, type: :model do
  it { should belong_to(:dog) }
  it { should validate_presence_of(:start_date) }
  it { should validate_presence_of(:end_date) }
  it { should validate_presence_of(:dog) }

  context 'when dog is reserved' do
    let(:existing_reservation) { create(:reservation) }
    subject { build(:reservation, dog: existing_reservation.dog) }
    it { should_not be_valid }
  end

  context 'when dog is not reserved' do
    subject { build(:reservation, dog: create(:dog)) }
    it { should be_valid }
  end
end
