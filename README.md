# Dog Running

This is a code test for a job interview. The requirements can be found on `docs/_Code Challege.pdf`

# Setup

1. Install ruby 2.6.0.
2. Run bundle
3. Create a new file named `database.yml` from `database.yml.example`
   with your postgres DB config
4. Enable caching in dev mode `rails dev:cache`
5. Start redis `redis-server`
6. On a separate terminal run `rails s`

# Running tests

Run `rspec`

# Endpoints documentation

## Get Breeds:

```
curl -X GET http://localhost:3000/api/v1/breeds
```

## Get Dogs:

```
curl -X GET 'http://localhost:3000/api/v1/dogs?sort=desc%28name%29&page=1&reserved=true'
```

NOTE: The query params available are:
* name: string, will search similar dog names
* age: positive int, will search dogs with this age
* weight: positive int, will search dogs with this weight
* reserved: true or false, if true will search for reserved dogs, else
  will search for not reserved dogs
* sort: can sort by mutliple columns, need to specify ordering type and column
  Valid ordering types:
    - asc
    - desc
  Valid columns:
    - name
    - breed
    - age
    - weight
  Examples:
    - asc(name)
    - desc(breed),asc(age)
* page: indicates page number, positive value else will go to first page
* per_page: indicates amount of dogs per page, if value is negative or zero or not specified defaults to 10

## Create Dog

```
curl -X POST http://localhost:3000/api/v1/dogs
  -H 'Content-Type: application/json' \
  -d '{ "dog": { "name": "Totti", "breed_id": 24, "age": 3 } }'
```

Accepted attributes inside dog attribute are:
* name (required)
* age (required)
* breed_id (required)
* weight (optional)
* reservation_date_limit (optional)

## Update Dog

```
curl -X PUT http://localhost:3000/api/v1/dogs/3
  -H 'Content-Type: application/json' \
  -d '{ "dog": { "weight": 300, "name": "Scrappy" } }'
```

Same accepted attributes inside dog as `Create Dog` endpoint, except no
attribute is required

## Read Dog

```
curl -X GET http://localhost:3000/api/v1/dogs/<dog_id>
```

Replace <dog_id> with a valid dog id, from the `Get Dogs` endpoint

## Delete Dog

```
curl -X DELETE http://localhost:3000/api/v1/dogs/<dog_id>
```

Replace <dog_id> with a valid dog id, from the `Get Dogs` endpoint

## Create Reservation

```
curl -X POST http://localhost:3000/api/v1/reservations \
  -H 'Content-Type: application/json' \
  -d '{ "dog_id": "19968", "number_of_days": 2 }'
```

The dog will be reserved from today's date, until the date after adding
the specified number_of_days


## Notes

There is a postman collection with examples for each endpoint inside
`docs/Dog Running.postman_collection.json`

# Future improvements
Due to the nature of the project there are some improvements that can be
made, since they were not specified in the requirements I list them
here:

* Add more validation for input entries like max length on name or max
  values for number_of_days, age, weight.
* When Validating if a dog can be reserved, also check dog.reservation_date_limit.
* Allow age and weight filters in dog#index to use gte, gt, lte, lt filtering.
* Remove unused Rails generated code/comments.
* Remove ActiveStorage routes
* Add logging with request_id for better error tracking
* Setup prod Redis cache properties
* Add response headers in dog#index for previous and next page links
* Add validation for query params. Example: page does not exceeds max page
